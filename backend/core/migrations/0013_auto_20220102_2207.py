# Generated by Django 3.2.10 on 2022-01-02 22:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0012_alter_formationuseperiod_match'),
    ]

    operations = [

        migrations.SeparateDatabaseAndState(

            database_operations=[
                migrations.RunSQL(
                    sql='ALTER TABLE core_team_studies '
                        'RENAME TO core_subscription',
                    reverse_sql='ALTER TABLE core_subscription '
                                'RENAME TO core_team_studies',
                ),
            ],

            state_operations=[
                migrations.CreateModel(
                    name='Subscription',
                    fields=[
                        (
                            'id',
                            models.BigAutoField(auto_created=True,
                                                primary_key=True,
                                                serialize=False,
                                                verbose_name='ID',
                                                ),

                        ),

                        (
                            'team',
                            models.ForeignKey(
                                on_delete=django.db.models.deletion.CASCADE,
                                to='core.Team',
                            ),
                        ),
                        (
                            'study',
                            models.ForeignKey(
                                on_delete=django.db.models.deletion.CASCADE,
                                to='core.Study',
                            ),
                        ),
                    ],
                ),

                migrations.AlterField(
                    model_name='team',
                    name='studies',
                    field=models.ManyToManyField(
                        to='core.Study',
                        through='core.Subscription',
                    ),
                ),
            ]
        ),

        migrations.AddField(
            model_name='subscription',
            name='last_update',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
