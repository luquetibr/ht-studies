from modeltranslation.translator import register, TranslationOptions
from .models import Study, Data


@register(Study)
class StudyTranslationOptions(TranslationOptions):
    fields = ("name", "description")


@register(Data)
class DataTranlastionOptions(TranslationOptions):
    fields = ("name",)
