import pathlib

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from ...models import Study


class Command(BaseCommand):
    help = 'Run the specified study'

    def add_arguments(self, parser):
        parser.add_argument('study_codename', type=str)

    def handle(self, *args, **options):

        study_codename = options["study_codename"]

        try:
            study = Study.objects.get(codename=study_codename)

        except Study.DoesNotExist:
            raise CommandError(f"Study codename '{study_codename}' "
                               f"does not exist")

        # Run study
        study.run()

        # Export data to csv
        study.export_to_csv_and_zip()

        self.stdout.write(
            self.style.SUCCESS(f"Study : {study.name} ran successfully"))
