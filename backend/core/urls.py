from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('privacy', views.privacy, name='privcay'),

    path('connect', views.connect, name='connect'),
    path('disconnect', views.disconnect, name='disconnect'),

    path('chpp_callback',
         views.chpp_callback,
         name='chpp_callback',
         ),

    path('app_data', views.app_data, name='app_data'),
    path('app_data/team_<int:team_id>', views.app_data, name='app_data'),

    path('subscribe/team_<int:team_id>/study_<int:study_id>',
         views.subscribe,
         name='subscribe',
         ),

    path('unsubscribe/team_<int:team_id>/study_<int:study_id>',
         views.unsubscribe,
         name='unsubscribe',
         ),

    path('download/study_<str:study_codename>/data',
         views.download_study,
         name='download_study',
         ),
]
